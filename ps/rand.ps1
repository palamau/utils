# A simple script that prints a random paragraph from
# a user-chosen text file in a text box.  Wrote this
# so that someone could do this using a GUI

# How to use it :
# Create an input file with text snippets of your choice
# Separate these snippets with a whitespace-only line
# The program will keep displaying random snippets from
# the file you choose.  The "snippets" file must have a 
# ".txt" extension - this is by-design


# Known issues 
# TODO: Not sure if the regex I use for splitting is correct - it probably isn't.
# TODO: GUI properties are hard-coded - probably ok for our purposes

Function Get-FileName($initialDirectory)
{
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
    $openFileDialog = New-Object System.Windows.Forms.OpenFileDialog
    $openFileDialog.initialDirectory = $initialDirectory
    $openFileDialog.filter = "TXT (*.txt)| *.txt"
    if ($openFileDialog.ShowDialog() -eq [System.Windows.Forms.DialogResult]::OK) {
        $openFileDialog.Filename
    }
}

Add-Type -AssemblyName System.Windows.Forms 
Add-Type -AssemblyName System.Drawing
$form = New-Object system.Windows.Forms.Form
$form.size = New-Object system.drawing.size(600,600)


$button = New-Object System.Windows.Forms.Button
$button.Location = New-Object System.Drawing.Size(35,35)
$button.Size = New-Object System.Drawing.Size(120,23)
$button.Text = "Next"

$inputfile = Get-FileName $env:PSHOME
if ($inputfile -eq $null ){
    exit(0)
}

$questions = ((get-content -raw -LiteralPath $inputfile ) -split "`n\s*`r+") 
$textbox = New-Object System.Windows.Forms.RichTextBox
$textbox.Size = New-Object System.Drawing.Size(470,170)
$textbox.Location = New-Object System.Drawing.Size(60,100)

$textbox.AutoSize = $True
$textbox.Multiline = $True
$textbox.Text = ($questions | Get-Random)
$form.Controls.Add($textbox)

$button.Add_Click(
    { 
        $textbox.Text = ($questions | Get-Random)
    }
)
$form.Controls.Add($button)
$form.ShowDialog()
